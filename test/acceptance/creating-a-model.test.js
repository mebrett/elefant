import { string, integer } from '../../src/elefant/attribute-types'
import { PERSISTED, FAILED, PENDING } from '../../src/elefant/sink-statuses'
import { CREATE } from '../../src/elefant/action-types'

import Model from '../../src/elefant/model'
import Store from '../../src/elefant/store'
import JsonApiSink from '../../src/elefant/sinks/json-api-sink'
import LocalStorageSink from '../../src/elefant/sinks/json-api-sink'

const server = { respond: () => null }

// Define sinks

class ApiSink extends JsonApiSink {
  static baseUrl() { return 'http://localhost:3000' }
  static downstreams() { return [LocalSink] }
}

class LocalSink extends LocalStorageSink {
  static upstreams() { return [ApiSink] }
}

// Define the models

class Planet extends Model {
  static attributes(){ return {
    name: string(),
    size: integer({ default: 10 })
  } }

  static relationships() { return { moons: hasMany() } }
  static grow(attrs) { return attrs.set('size', attrs.get('size') + 1) }
  static shrink(attrs) { return attrs.set('size', attrs.get('size') - 1) }
}

// Define the store

class SolarSystemStore extends Store {
  static primarySink() { return LocalSink }
  static models() { return { Planet, Moon: null } }
}

// Tests

it("Creates a model", () => {
  step("I initialize my store")
  const { dispatch, fetch, listen } = new SolarSystemStore()

  step("I fetch my initial state and it is empty")
  const initialState = fetch()
  const expectedInitialState = { collections: { Planet: [], Moon: [] } }
  expect(initialState).to.deep.equal(expectedInitialState)

  step("I dispatch a Planet.create event")
  dispatch({
    model: Planet, type: CREATE,
    attributes: { name: 'Earth', size: 100 }
  })

  return 'PENDING'

  step("I fetch the state and the planet exists but the ApiSink is still pending")
  const newState = fetch()
  const expectedState = {
    collections: {
      Moon: [],
      Planet: [ {
        guid: 'abcde',
        attributes: { name: 'Earth', size: 100 },
        sinks: { LocalSink: PERSISTED, ApiSink: PENDING }
      } ]
    }
  }
  expect(newState).to.deep.equal(expectedState)

  step("The server responds with some additional attributes")
  server.respond()

  step("I fetch the state, and the planet exists, with new data, and ApiSink PERSISTED")
  const finalState = fetch()
  const expectedFinalState = {
    collections: {
      Moon: [],
      Planet: [ {
        guid: 'abcde',
        attributes: { name: 'Earth', size: 100, distance: 123 },
        sinks: { LocalSink: PERSISTED, ApiSink: PERSISTED }
      } ]
    }
  }
  expect(finalState).to.deep.eql(expectedFinalState)
})
