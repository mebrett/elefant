import loglevel from 'loglevel'
import chai, { expect } from 'chai'
import spies from 'chai-spies'

chai.use(spies)
loglevel.setLevel(process.env.LOG_LEVEL || 'info')

global.step   = loglevel.info
global.expect = expect
global.spy    = chai.spy
