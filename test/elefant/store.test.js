import Store from '../../src/elefant/store'
import { CREATE } from '../../src/elefant/action-types'

const MockModel = {}

describe("Store", () => {
  describe("construction", () => {
    it("creates a store with fetch, dispatch and listen functions", () => {
      const result = new Store()

      expect(result.fetch).to.be.a('function')
      expect(result.dispatch).to.be.a('function')
      expect(result.listen).to.be.a('function')
    })
  })

  describe(".dispatch", () => {
    it("dispatches an action", () => {
      MockModel.create = spy()
      const store = new Store()
      const result = store.dispatch({ model: MockModel, type: CREATE })
      expect(MockModel.create).to.have.been.called()
    })
  })

  describe(".fetch", () => {
    it("fetches the current state", () => {
      const store = new Store()
      const result = store.fetch()

      expect(result).to.deep.eql({ collections: {} })
    })
  })

  describe(".listen", () => {
    it("takes a function and calls it with new state after an action is dispatched", () => {
      const store = new Store()
      const mockCallback = spy()
      store.listen(mockCallback)
      store.dispatch({ type: 'bogus' })
      expect(mockCallback).to.have.been.called()
    })
  })
})
