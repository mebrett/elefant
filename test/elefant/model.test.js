import Model from '../../src/elefant/model'

describe("Model", () => {
  describe(".create", () => {
    it("returns a model with a guid and attributes", () => {
      const result = Model.create({ name: 'Test' })

      expect(result).to.deep.eql({
        guid: 'abcde', attributes: { name: 'Test' }
      })
    })
  })
})
