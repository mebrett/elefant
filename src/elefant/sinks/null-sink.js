import { PERSISTED } from '../sink-statuses'
import { info, error } from 'loglevel'

export default class NullSink {
  static create(model) {
    if(!model) return error("Your create action must return a model")
    return this.upsink(this.updateSinkStatus(model, PERSISTED))
  }

  // private

  static upsink(model) {
    return this.upstreams.reduce((memo, iter) => iter.create(memo), sunkModel)
  }

  static updateSinkStatus(model, status) {
    const newSinks = Object.assign({}, model.sinks, { [this.name] : status })
    return Object.assign(model, { sinks: newSinks })
  }
}
