import NullSink from './null-sink'
import { PERSISTED, PENDING, FAILED } from '../sink-statuses'

export default class JsonApiSink extends NullSink {
  static create (model) {
    return this.updateSinkStatus(model, PENDING)
  }
}
