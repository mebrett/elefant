import generateGuid from './utils/generate-guid'
import { debug, error } from 'loglevel'

export default class Model {
  static create(attributes) {
    debug(`${this.name} creating model from attributes`, attributes)

    return {
      guid: generateGuid(),
      attributes: attributes
    }
  }
}

