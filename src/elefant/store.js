import { debug, info, warn, error } from 'loglevel'
import { PERSISTED, PENDING, FAILED } from './sink-statuses'
import { CREATE } from './action-types'
import NullSink from './sinks/null-sink'

export default class Store {
  static models() { return {} }
  static upstreams() { return [] }
  static downstreams() { return [] }
  static primarySink() { return NullSink }

  constructor () {
    debug(`${this.constructor.name} creating new store`)

    this.state = { collections: this.initializeCollections() }
    this.listeners = []

    this.fetch = this.fetch.bind(this)
    this.dispatch = this.dispatch.bind(this)
    this.performCreate = this.performCreate.bind(this)
  }

  dispatch(action) {
    debug(`${this.constructor.name} dispatching action`, action)

    if ( action.type === CREATE ) this.performCreate(action)

    this.broadcastState()
  }

  fetch() { return this.state }

  listen(callback) { this.listeners = this.listeners.concat(callback) }

  setState() {}

  // private

  findName(model) { return 'Planet' }

  performCreate (action) {
    let name       = this.findName(action.model),
        collection = this.state.collections[name] || [],
        model      = action.model.create(action.attributes)

    debug(`${this.constructor.name} created model`, model)
    const sunkModel = this.constructor.primarySink().create(model)

    collection.push(sunkModel)
    this.state.collections[name] = collection
  }

  broadcastState() {
    const newState = this.fetch()
    debug(`${this.constructor.name} broadcasting new state`, newState)

    this.listeners.map((callback) => callback(newState))
  }

  initializeCollections() {
    return Object.keys(
      this.constructor.models()
    ).reduce((mem, iter) => {
      mem[iter] = []
      return mem
    }, {})
  }
}
